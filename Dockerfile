FROM maven:3.8.5-eclipse-temurin-8-alpine AS build
RUN apk add git
RUN git clone https://gitlab.com/ot-interview/javaapp.git /java-app
WORKDIR /java-app/
RUN mvn clean package

FROM tomcat:8.5.81-jre8-openjdk-slim-buster
RUN rm -rf /usr/local/tomcat/webapps/*
COPY --from=build /java-app/target/Spring3HibernateApp.war /usr/local/tomcat/webapps/ROOT.war
WORKDIR /usr/local/tomcat/webapps/
EXPOSE 8080
